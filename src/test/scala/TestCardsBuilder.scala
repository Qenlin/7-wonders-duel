import pl.axionis.sevenwondersduel.card._

object TestCardsBuilder {

  def getStoneSourceCard(): Card =
    Card("Składowisko kamienia", "Resource", CostInMoney(Money(1)), MaterialsProduction(Seq(Stone())))

  def getClaySourceCard(): Card =
    Card("Glinianka", "Resource", NoCost(), MaterialsProduction(Seq(Clay())))

  def getPapyrusSourceCard(): Card =
    Card("Wytwórnia papirusu", "Goods", CostInMoney(Money(1)), MaterialsProduction(Seq(Papyrus())))

}
