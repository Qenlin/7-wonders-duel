package pl.axionis.sevenwondersduel.deck

case class DeckNode(isHidden: Boolean, leftChildren: Option[DeckNode], rightChildren: Option[DeckNode])