package pl.axionis.sevenwondersduel.card

case class Card(name:String, cardType:String, cost: Cost, production: Production)

case class Money(money: Int)
