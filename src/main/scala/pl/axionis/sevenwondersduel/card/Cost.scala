package pl.axionis.sevenwondersduel.card

trait Cost

case class CostInMaterials(materials: Seq[Material])extends Cost

case class CostInMoney(money: Money) extends Cost

case class NoCost()extends Cost