package pl.axionis.sevenwondersduel.card

trait Production

case class MaterialsProduction(materials: Seq[Material]) extends Production

case class MoneyProduction(money: Money) extends Production
