package pl.axionis.sevenwondersduel.card

trait Material

case class Wood() extends Material

case class Clay() extends Material

case class Stone() extends Material

case class Papyrus() extends Material

case class Glass() extends Material
