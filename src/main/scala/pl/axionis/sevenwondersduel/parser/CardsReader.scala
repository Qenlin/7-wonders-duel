package pl.axionis.sevenwondersduel.parser

import play.api.libs.json.Json

import scala.io.BufferedSource

trait CardsReader
object CardsReader extends CardsReader{

  def readCardsFromFile(source: BufferedSource) = {
    val t = Json.parse(source.mkString)
    t
  }
}
