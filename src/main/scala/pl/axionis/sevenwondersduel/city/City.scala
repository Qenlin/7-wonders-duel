package pl.axionis.sevenwondersduel.city

import pl.axionis.sevenwondersduel.card.{Money, Production}

case class City(production: Seq[Production], money: Money)